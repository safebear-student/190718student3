package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        //Step 1: Confirm the welcome page is displayed
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2: Click on login link
        welcomePage.clickOnLogin();

        //Step 3: Confirm the user is in the login screen
        assertTrue(loginPage.checkCorrectPage());

        // Enter the username and password and click the Return key
        loginPage.login("testuser", "testing");

        //Confirm the user is directed t the User page
        assertTrue(userPage.checkCorrectPage());



    }
}

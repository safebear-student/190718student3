package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {
    private WebDriver driver;
    private String url;
    private String browser;
    Properties prop = new Properties();
    InputStream input = null;

    public Utils(){
        try {
          String file = "config.properties";
          input = Utils.class.getClassLoader().getResourceAsStream(file);
          if (input==null){
              System.out.println("Unable to find " + file);
              return;
          }
          prop.load(input);

          this.url = prop.getProperty("url");
          this.browser = prop.getProperty("browser");


        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input!=null) {
                try {
                    input.close();

                    } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        switch (browser) {
            case "chrome":
                this.browser = new ChromeDriver();
                break;
            default:
                this.browser = new ChromeDriver();
                break;

        }

    }

    public WebDriver getDriver(){

        return driver;
    }

    public String getUrl() {
        return url;
    }
}



package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;


   @Before
   public void setUp() {
       //driver = new ChromeDriver();
       ChromeOptions chromeOptions = new ChromeOptions();
       chromeOptions.addArguments("headless");
       this.driver = new ChromeDriver(chromeOptions);
       welcomePage = new WelcomePage(driver);
       loginPage = new LoginPage(driver);
       userPage = new UserPage (driver);


       // Open automate SafeBear.co.uk window
       driver.get("http://automate.safebear.co.uk/");
       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
       //driver.getTitle().toString();


   }

    @After
    public void tearDown() {

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Quit the driver
        driver.quit();

    }

}
